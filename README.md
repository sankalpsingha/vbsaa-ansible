# sankalpsingha/Quadtrust-Ansible-Server

## The basic Gist of it :

[Ansible](http://docs.ansible.com/index.html) playbook to provision a Rails deployment server with:

* Ubuntu 14.04.1
* MySql
* rbenv
* ruby 2.3.0
* [Phusion Passenger](https://www.phusionpassenger.com/) + nginx from Phusion's apt repo
* Prepares an nginx vhost for a Rails app, ready for deployment with `cap` or `mina` ( I prefer mina )


## Overview

These are the main things that it does : 

* Adds all the linus Prereqs
* Adds basic UFW firewall
* Adds my sql + rbenv + ruby + nginx-passenger 
* Syncing the TIMEZONE and adding NTP 
* Setting a FQDN ( This is required to be proper for the Postfix setup )
* Adding Postfix! ( Please note that if you are using Mandrill to send the emails then, the domain needs to be verified. )

Following various sources to create an idempotent, repeatable provisioning script for Rails applications (among other things) using Ansible.

My current goal is to be able to deploy a Rails / MySql / Redis app to DigitalOcean in "one click". Perhaps "many clicks", but as minimal human intervention as possible.

Repeatably. Possibly even including provisioning of the cloud instances.

At the very least, to create a consistent set of DEV, STAGING and PROD environments.

Benefits to using something like Ansible to manage servers:

* Reduce "[jenga](https://www.youtube.com/watch?v=I7H6wGy5zf4)" feeling when running servers (same win as unit testing and source control)
* Consistency between different environments
* Executable documentation
* Combined with cloud providers and hourly billing, can create on-demand staging environments



###  To run individual roles (e.g. only install nginx), try the following. You can replace `nginx` with any role name.

```
ansible-playbook build-server.yml -i hosts --tags nginx
```


* Generate a crypted password, if not already done, and put it in `vars/default.yml`. This will be the password for your `deploy` user.

```
python support/generate-crypted-password.py
```

Now, run the playbook as usual. Good luck!

```
ansible-playbook build-server.yml -i hosts -u root -K -vvvv
```

Note that after the first run, `root` will no longer be able to log in. To run the playbook again, replace `root` with the `deploy` user as set in `vars/defaults.yml`.

* Also note that you might have to add the `--ask-sudo-pass` as well in the command when you use it for the deploy user. 

```
ansible-playbook provision-app.yml -i hosts -u deploy -vv --ask-sudo-pass
```

## Notes


* It seems so. From googling, it seems Ansible [is not designed to be run with the sudoers file only allowing pre-approved commands.](https://serverfault.com/questions/560106/how-can-i-implement-ansible-with-per-host-passwords-securely)

* It seems just having a `deploy` user is a bad idea. It's messy. Perhaps there should be a `provision` user as well. This user could install packages, etc. and `deploy` should only affect have rights to the app in `~/deploy`.

* For now, keep it simple, just `deploy`. But we won't go down the `NOPASSWD` route. It's too risky. Consider: if there's any bug in Rails that allowed remote code execution, attackers are one `sudo` away from full control.

* The pros of disabling `NOPASSWD` outweigh the cons. Should make it standard practice when creating an Ansible playbook: create a `deploy` user with a unique password, use that password for sudo, and always pass `-K` when necessary. Design playbooks to be clear about whether it requires sudo or not. Right now it's very muddy with privileged and unprivileged tasks combined in the same play.

* If you get the following error installing ansible on OS X Mavericks:

```
# clang: error: unknown argument: '-mno-fused-madd' [-Wunused-command-line-argument-hard-error-in-future]
```

Run: 

```
echo "ARCHFLAGS=-Wno-error=unused-command-line-argument-hard-error-in-future" >> ~/.zshrc
. ~/.zshrc
brew install ansible
```

See [Stack Overflow question](https://stackoverflow.com/questions/22390655/ansible-installation-clang-error-unknown-argument-mno-fused-madd) for details.


## Issues with Passenger - solved

There's two ways to install Passenger:

* Using their official Ubuntu packages, which installs Passenger files in various locations throughout the system (n.b. as do all apt packages)
* Your `passenger_root` must be a `locations.ini` file which points to all these locations. This ships with the passenger package and can also be generated with one of the `passenger-config about` commands.

Or...

* `gem install passenger` (or adding `passenger` to `Gemfile`), which contains all Passenger files in your rubygems directory.
* `install-passenger-nginx-module`, which compiles a brand new nginx and installs it into `/opt/nginx`. [Nginx modules must be statically loaded.](https://github.com/phusion/passenger/wiki/Why-can't-Phusion-Passenger-extend-my-existing-Nginx%3F).

The former makes more sense, since it's apt it seems cleaner/more maintainable. Especially for nginx, I don't want to have to be recompiling if I need a new module or security update.

I spent a long time trying to figure out why nginx returned `403 Forbidden` for anything not in `./public`.

I was defining `passenger_ruby` in my server-specific config, but forgetting `passenger_root`. I ended up putting these two lines in `/etc/nginx/nginx.conf`, and now all is well. In my server specific config, I only have `passenger_enabled`.

## Things that still need fixing

* Security: 5minbootstrap

## Fixed issues / answered questions

* There might be tons, please open issues if as and when you do.


## Sources / references

Synthesized from the following sources:

* [From Zero to Deployment: Vagrant, Ansible, Capistrano 3 to deploy your Rails Apps to DigitalOcean automatically (part 1)](http://ihassin.wordpress.com/2013/12/15/from-zero-to-deployment-vagrant-ansible-rvm-and-capistrano-to-deploy-your-rails-apps-to-digitalocean-automatically/)
* [leucos/ansible-tuto](https://github.com/leucos/ansible-tuto)
* [leucos/ansible-rbenv-playbook](https://github.com/leucos/ansible-rbenv-playbook)
* [dodecaphonic/ansible-rails-app](https://github.com/dodecaphonic/ansible-rails-app/)
* [Ansible: List of All Modules](http://docs.ansible.com/list_of_all_modules.html) (easiest way to find module docs, CMD+F / CTRL+F)
* [Phusion Passenger users guide, Nginx edition](http://www.modrails.com/documentation/Users%20guide%20Nginx.html)


## How to run the backup

```
#  ansible-playbook backup-application.yml -i hosts -u deploy --ask-sudo-pass -vvvv   
```

